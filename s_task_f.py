#!/usr/bin/env python
# -*- coding: utf-8 -*-


import boto3
import json
from botocore.client import ClientError

REGION_NAME = 'us-west-2'
TRANSCODER_ROLE_NAME = 'Elastic_Transcoder_Default_Role'
PIPELINE_NAME = 'test-pipeline'
IN_BUCKET_NAME = 'test-sns99999'
OUT_BUCKET_NAME = 'test-sns99990'
INPUT_KEY = 'video.mp4'  
TOPIC_NAME = 'SuccessfulConversion'
FILE_UPLOAD_NAME = 'video_source.mp4'
FILE_UP_OUTPUT_NAME = 'video.mp4'


def main():
    transcoder = boto3.client('elastictranscoder', REGION_NAME)
    s3 = boto3.resource('s3')
    iam = boto3.resource('iam')
    sns = boto3.resource('sns', REGION_NAME)
    s3_client = boto3.client('s3')

    def check_role(role_name):
        try:
            iam.meta.client.get_role(RoleName=role_name)
        except ClientError:
            raise

    check_role(TRANSCODER_ROLE_NAME)
    role = iam.Role(TRANSCODER_ROLE_NAME)

    def check_bucket(bucket_name):
        try:
           s3.meta.client.head_bucket(Bucket=bucket_name)
        except ClientError:
            logger.error("Bucket does not exist. bucket_name={}".format(bucket_name))
            raise

    check_bucket(IN_BUCKET_NAME)
    check_bucket(OUT_BUCKET_NAME)
    
    s3_client.upload_file(FILE_UPLOAD_NAME,IN_BUCKET_NAME,FILE_UP_OUTPUT_NAME)
    topic_arn = sns.create_topic(Name=TOPIC_NAME).arn

    response = transcoder.create_pipeline(
        Name=PIPELINE_NAME,
        InputBucket=IN_BUCKET_NAME,
        OutputBucket=OUT_BUCKET_NAME,
        Role=role.arn,
    )
    # print("response={}".format(response))
    pipeline_id = response['Pipeline']['Id']

    job = transcoder.create_job(
        PipelineId=pipeline_id,
        Input={
            'Key': INPUT_KEY,
            'FrameRate': 'auto',
            'Resolution': 'auto',
            'AspectRatio': 'auto',
            'Interlaced': 'auto',
            'Container': 'auto',
        },
        Outputs=[
            {
                'Key': 'OGG/Files/{}.ogg'.format('.'.join(INPUT_KEY.split('.')[:-1])),
                'PresetId': '1351620000001-100070',  # System preset: Web                
            },
        ],
    )
    print("job={}".format(job))
    job_id = job['Job']['Id']
    
    waiter = transcoder.get_waiter('job_complete')
    waiter.wait(Id=job_id)
        
    inp_key = IN_BUCKET_NAME +"/"+ INPUT_KEY
    out_key = OUT_BUCKET_NAME +"/"+ 'OGG/Files/{}.ogg'.format('.'.join(INPUT_KEY.split('.')[:-1]))
    message = {"input": inp_key, "output": out_key}
    json_message = json.dumps({"default":json.dumps(message)})

    response = sns.Topic(topic_arn).publish(
        Subject="AWS Transcoder job has finished successfully.",
        MessageStructure="json",
        Message="{}".format(json_message),
    )


if __name__ == '__main__':
    main()
