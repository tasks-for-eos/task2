Test #2

1) upload a file to s3
2) call to transcode to process the file
3) wait for transcode to finish
4) call to sns and publish a message with following json:
{
"input": "key where uploaded on s3",
"output": "key where transcoder stored file"
}